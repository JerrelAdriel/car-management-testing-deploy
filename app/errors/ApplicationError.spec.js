const ApplicationError = require("./ApplicationError");

describe("ApplicationError", () => {
  describe("#getDetails", () => {
    it("should return empty object", async () => {
      const err = new ApplicationError("Something Error");

      expect(err).toBeInstanceOf(Error);
      expect(Object.getPrototypeOf(err.details)).toEqual(Object.prototype);
    });

    describe("#toJSON", () => {
      it("should return error", async () => {
        const err = new ApplicationError("Something Error");
        const mockReturnValue = jest.fn(() => err.toJSON());
        mockReturnValue();

        expect(mockReturnValue).toHaveReturned();
        expect(mockReturnValue).toHaveReturnedWith({
          error: {
            name: "Error",
            message: "Something Error",
            details: {},
          },
        });
      });
    });
  });
});
