const CarController = require("./CarController");
const { Car, UserCar } = require("../models");
const dayjs = require("dayjs");
const { Op } = require("sequelize");
const CarAlreadyRentedError = require("../errors/CarAlreadyRentedError");


describe("CarController", () => {
  describe("handleListCars", () => {
    it("should return res.status(200) and a list of cars instance", async () => {
      const req = {
        query: {
          size: "SMALL",
          availableAt: new Date(),
        },
      };
      const cars = [];
      const name = "Honda Civic";
      const price = "500000";
      const size = "small";
      const image = "civic.jpg";
      const isCurrentlyRented = false;

      for (let i = 0; i < 10; i++) {
        const car = new Car({ name, price, size, image, isCurrentlyRented });
        cars.push(car);
      }

      const mockCarModel = {
        findAll: jest.fn().mockReturnValue(cars),
        count: jest.fn().mockReturnValue(10),
      };
      const mockUserCar = new UserCar({
        userId: 1,
        carId: 1,
        rentStartedAt: new Date(),
        rentEndedAt: new Date(),
      });

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCar,
        dayjs,
      });

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      await carController.handleListCars(req, res);

      expect(mockCarModel.findAll).toHaveBeenCalled();
      expect(mockCarModel.count).toHaveBeenCalled();
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        cars,
        meta: {
          pagination: {
            page: 1,
            pageCount: 1,
            pageSize: 10,
            count: 10,
          },
        },
      });
    });
  });

  describe("handleGetCar", () => {
    it("should return res.status(200) and a car", async () => {
      const mockCar = new Car({
        id: 1,
        name : "Honda Civic",
        price : "500000",
        size : "small",
        image : "civic.jpg",
        isCurrentlyRented: false,
      });
      const mockCarModel = {
        findByPk: jest.fn().mockReturnValue(mockCar),
      };

      const mockUserCar = new UserCar({
        userId: 1,
        carId: 1,
        rentStartedAt: new Date(),
        rentEndedAt: new Date(),
      });

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCar,
        dayjs,
      });

      const req = {
        params: {
          id: 1,
        },
      };
      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      await carController.handleGetCar(req, res);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(req.params.id);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(mockCar);
    });
  });

  describe("handleCreateCar", () => {
    it("should return res.status(201) and a car", async () => {
        const mockCar = new Car({
          id: 1,
          name : "Honda Civic",
          price : "500000",
          size : "small",
          image : "civic.jpg",
          isCurrentlyRented: false,
        });
        const mockCarModel = {
          create: jest.fn().mockReturnValue(mockCar),
        };
        const mockUserCar = new UserCar({
          userId: 1,
          carId: 1,
          rentStartedAt: new Date(),
          rentEndedAt: new Date(),
        });
  
        const carController = new CarController({
          carModel: mockCarModel,
          userCarModel: mockUserCar,
          dayjs,
        });
  
        const req = {
          body: {
            name : "Honda Civic",
            price : "500000",
            size : "small",
            image : "civic.jpg",
          },
        };
  
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
  
        await carController.handleCreateCar(req, res);
  
        expect(mockCarModel.create).toHaveBeenCalledWith({
          ...req.body,
          isCurrentlyRented: false,
        });
        expect(res.status).toHaveBeenCalledWith(201);
        expect(res.json).toHaveBeenCalledWith(mockCar);
      });
    it("should return res.status(422) if there's an error", async () => {
      const mockCarModel = {
        create: jest.fn(() => Promise.reject(err)),
      };
      const mockUserCar = new UserCar({
        userId: 1,
        carId: 1,
        rentStartedAt: new Date(),
        rentEndedAt: new Date(),
      });

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCar,
        dayjs,
      });

      const req = {
        body: {
           name : "Honda Civic",
           price : "500000",
           size : "small",
           image : "civic.jpg",
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      await carController.handleCreateCar(req, res);

      expect(mockCarModel.create).toHaveBeenCalledWith({
        ...req.body,
        isCurrentlyRented: false,
      });
      expect(res.status).toHaveBeenCalledWith(422);
      expect(res.json).toHaveBeenCalledWith({
        error: {
          name: "error",
          message: "error",
        },
      });
    });
  });

  describe("#handleRentCar", () => {
    it("should return res.status(201) and res.json with userCar instance", async () => {
      const id = 1;
      const name = "Honda Civic"
      const price = "500000"
      const size = "small"
      const image = "civic.jpg"
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        params: {
          id: 1,
        },
        body: {
          rentStartedAt,
          rentEndedAt,
        },
        user: {
          id: 1,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockNext = jest.fn();

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });
      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });

      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = {
        findOne: jest.fn().mockReturnValue(null),
        create: jest.fn().mockReturnValue({
          userId: mockUserCar.userId,
          carId: mockUserCar.carId,
          rentStartedAt: mockUserCar.rentStartedAt,
          rentEndedAt: mockUserCar.rentEndedAt,
        }),
      };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleRentCar(mockRequest, mockResponse, mockNext);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockUserCarModel.findOne).toHaveBeenCalledWith({
        where: {
          carId: mockCar.id,
          rentStartedAt: {
            [Op.gte]: mockRequest.body.rentStartedAt,
          },
          rentEndedAt: {
            [Op.lte]: mockRequest.body.rentEndedAt,
          },
        },
      });
      expect(mockUserCarModel.create).toHaveBeenCalledWith({
        userId: mockRequest.user.id,
        carId: mockCar.id,
        rentStartedAt: mockRequest.body.rentStartedAt,
        rentEndedAt: mockRequest.body.rentEndedAt,
      });
      expect(mockResponse.status).toHaveBeenCalledWith(201);
      expect(mockResponse.json).toHaveBeenCalledWith({
        userId: mockUserCar.userId,
        carId: mockUserCar.carId,
        rentStartedAt: mockUserCar.rentStartedAt,
        rentEndedAt: mockUserCar.rentEndedAt,
      });
    });
    it("should call the next function with error", async () => {
      const err = new Error("Somthing Error");
      const id = 1;
      const name = "Honda Civic";
      const price = "500000";
      const size = "small";
      const image = "civic.jpg";
      const isCurrentlyRented = false;

      const mockRequest = {
        params: {
          id: 1,
        },
        body: {
          rentStartedAt: new Date(),
          rentEndedAt: new Date(),
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockNext = jest.fn();

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });

      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = { findOne: jest.fn(() => Promise.reject(err)) };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleRentCar(mockRequest, mockResponse, mockNext);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockUserCarModel.findOne).toHaveBeenCalledWith({
        where: {
          carId: mockCar.id,
          rentStartedAt: {
            [Op.gte]: mockRequest.body.rentStartedAt,
          },
          rentEndedAt: {
            [Op.lte]: mockRequest.body.rentEndedAt,
          },
        },
      });
      expect(mockNext).toHaveBeenCalled();
    });
  });
  describe("#handleUpdateCar", () => {
    it("should return res.status(201) and res.json with car instance", async () => {
      const name = "Honda Civic";
      const price = "500000";
      const size = "small";
      const image = "civic.jpg";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        params: {
          id: 1,
        },
        body: {
          name,
          price,
          size,
          image,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockCar = new Car({ name, price, size, image, isCurrentlyRented });
      mockCar.update = jest.fn().mockReturnThis();
      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });

      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = { mockUserCar };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleUpdateCar(mockRequest, mockResponse);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockCar.update).toHaveBeenCalledWith({ name, price, size, image, isCurrentlyRented });
      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith(mockCar);
    });
  });

  describe("#handleDeleteCar", () => {
    it("it should be return with res.status(204) and pseudo delete", async () => {
      const id = 1;
      const name = "Honda Civic";
      const price = "500000";
      const size = "small";
      const image = "civic.jpg";
      const isCurrentlyRented = false;

      const mockRequest = {
        params: {
          id: 1,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        end: jest.fn().mockReturnThis(),
      };

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });
      mockCar.destroy = jest.fn().mockReturnValue(mockCar);

      const carController = new CarController({ carModel: mockCar });
      await carController.handleDeleteCar(mockRequest, mockResponse);

      expect(mockCar.destroy).toHaveBeenCalledWith(1);
      expect(mockResponse.status).toHaveBeenCalledWith(204);
      expect(mockResponse.end).toHaveBeenCalledWith();
    });
  });

  describe("#getCarFromRequest", () => {
    it("should return car", async () => {
      const id = 1;
      const name = "Honda Civic";
      const price = "500000";
      const size = "small";
      const image = "civic.jpg";
      const isCurrentlyRented = false;

      const mockRequest = {
        params: {
          id: 1,
        },
      };

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });
      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };

      const carController = new CarController({ carModel: mockCarModel });
      await carController.getCarFromRequest(mockRequest);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
    });
  });

  describe("#getListQueryFromRequest", () => {
    it("should return object of query", async () => {
      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const query = {
        size: "SMALL",
        availableAt: new Date(),
      };

      const mockRequest = {
        query,
      };

      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });
      const mockUserCarModel = { mockUserCar };

      const carController = new CarController({ userCarModel: mockUserCarModel });
      const result = await carController.getListQueryFromRequest(mockRequest);

      const include = {
        model: mockUserCarModel,
        as: "userCar",
        required: false,
        where: {
          rentEndedAt: {
            [Op.gte]: query.availableAt,
          },
        },
      };

      expect(result).toStrictEqual({
        include,
        where: {
          size: query.size,
        },
        limit: 10,
        offset: 0,
      });
    });
  });
});
