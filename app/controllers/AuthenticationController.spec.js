const AuthenticationController = require("./AuthenticationController");
const { WrongPasswordError, EmailNotRegisteredError, InsufficientAccessError } = require("../errors");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { User, Role } = require("../models");
const { JWT_SIGNATURE_KEY } = require("../../config/application");

describe("AuthenticationController", () => {
  describe("#authorize", () => {
    it("should call next function", async () => {
      const id = 1;
      const name = "Jerrel";
      const email = "jerrel@binar.co.id";
      const image = "jerrel.jpg";

      const mockUser = new User({ id: id, name: name, email: email, image: image });

      const mockRole = new Role({ id: 2, name: "ADMIN" });

      const mockRequest = {
        headers: {
          authorization:
            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6ImZyaXphIiwiZW1haWwiOiJleGFtcGxlQGVtYWlsLmNvbSIsImltYWdlIjpudWxsLCJyb2xlIjp7ImlkIjoxLCJuYW1lIjoiQ1VTVE9NRVIifSwiaWF0IjoxNjU0OTM5MjY5fQ.J3zzqC14IbX-Ih0oVt8ThL00EVS5nTfgCP7CzFK0-bU",
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockNext = jest.fn();

      const authenticationController = new AuthenticationController({
        userModel: mockUser,
        roleModel: mockRole,
        bcrypt,
        jwt,
      });

      await authenticationController.authorize("CUSTOMER")(mockRequest, mockResponse, mockNext);

      expect(mockNext).toHaveBeenCalled();
    });

    it("should call res.status(401) and res.json with error", async () => {
      const id = 1;
      const name = "Jerrel";
      const email = "jerrel@binar.co.id";
      const image = "jerrel.jpg";

      const mockUser = new User({ id: id, name: name, email: email, image: image });

      const mockRole = new Role({ id: 1, name: "CUSTOMER" });

      const err = new InsufficientAccessError(mockRole.name);

      const mockRequest = {
        headers: {
          authorization:
            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6ImZyaXphIiwiZW1haWwiOiJleGFtcGxlQGVtYWlsLmNvbSIsImltYWdlIjpudWxsLCJyb2xlIjp7ImlkIjoxLCJuYW1lIjoiQ1VTVE9NRVIifSwiaWF0IjoxNjU0OTM5MjY5fQ.J3zzqC14IbX-Ih0oVt8ThL00EVS5nTfgCP7CzFK0-bU",
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockNext = jest.fn();

      const authenticationController = new AuthenticationController({
        userModel: mockUser,
        roleModel: mockRole,
        bcrypt,
        jwt,
      });
      await authenticationController.authorize("ADMIN")(mockRequest, mockResponse, mockNext);

      expect(mockResponse.status).toHaveBeenCalledWith(401);
      expect(mockResponse.json).toHaveBeenCalledWith({
        error: {
          name: err.name,
          message: err.message,
          details: err.details || null,
        },
      });
    });

    describe("#handleLogin", () => {
      it("should call res.status(404) and res.json with error instance if user not found", async () => {
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";

        const mockUserModel = { findOne: jest.fn().mockReturnValue(null) };

        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const mockRequest = {
          body: {
            email: email,
            password: password,
          },
        };

        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };

        const mockNext = jest.fn();

        const err = new EmailNotRegisteredError(mockRequest.body.email);

        const authenticationController = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
        await authenticationController.handleLogin(mockRequest, mockResponse, mockNext);

        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockRequest.body.email },
          include: [{ model: mockRole, attributes: ["id", "name"] }],
        });
        expect(mockResponse.status).toHaveBeenCalledWith(404);
        expect(mockResponse.json).toHaveBeenCalledWith(err);
      });

      it("should call res.status(401) and res.json with error instance if password is wrong", async () => {
        const err = new WrongPasswordError();
        const id = 1;
        const name = "Jerrel";
        const email = "jerrel@binar.co.id";
        const image = "jerrel.jpg";
        const password = "jerrel123";
        const roleId = 2;

        const mockUserModel = {
          findOne: jest.fn().mockReturnValue({
            id: id,
            name: name,
            email: email,
            image: image,
            encryptedPassword: "wrongpassword",
            roleId: roleId,
          }),
        };

        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const mockRequest = {
          body: {
            email: email,
            password: password,
          },
        };

        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };

        const mockNext = jest.fn();

        const authenticationController = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
        await authenticationController.handleLogin(mockRequest, mockResponse, mockNext);

        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockRequest.body.email.toLowerCase() },
          include: [{ model: mockRole, attributes: ["id", "name"] }],
        });
        expect(mockResponse.status).toHaveBeenCalledWith(401);
        expect(mockResponse.json).toHaveBeenCalledWith(err);
      });

      it("should call next function with error instance", async () => {
        const err = new Error("Something Error");
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";

        const mockUserModel = {
          findOne: jest.fn(() => Promise.reject(err)),
        };

        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const mockRequest = {
          body: {
            email: email,
            password: password,
          },
        };

        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };

        const mockNext = jest.fn();

        const authenticationController = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
        await authenticationController.handleLogin(mockRequest, mockResponse, mockNext);

        expect(mockNext).toHaveBeenCalled();
      });
    });

    describe("#handleRegister", () => {
      it("should call res.status(201) and res.json with accessToken", async () => {
        const id = 1;
        const name = "Jerrel";
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";
        const encryptedPassword = bcrypt.hashSync(password, 10);
        const image = "jerrel.jpg";
        const roleId = 1;

        const mockUser = new User({ id: id, name: name, email: email, image: image, password: encryptedPassword, roleId: roleId });

        const mockRole = new Role({ id: 1, name: "CUSTOMER" });

        const mockRequest = {
          body: {
            name,
            email,
            password,
          },
        };

        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };

        const mockNext = jest.fn();

        const mockUserModel = { create: jest.fn().mockReturnValue(mockUser), findOne: jest.fn().mockReturnValue(false) };

        const mockRoleModel = { findOne: jest.fn().mockReturnValue(mockRole) };

        const authenticationController = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRoleModel,
          bcrypt,
          jwt,
        });
        await authenticationController.handleRegister(mockRequest, mockResponse, mockNext);

        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockRequest.body.email },
        });
        expect(mockUserModel.create).toHaveBeenCalled();
        expect(mockRoleModel.findOne).toHaveBeenCalledWith({
          where: { name: mockRole.name },
        });
        expect(mockResponse.status).toHaveBeenCalledWith(201);
        expect(mockResponse.json).toHaveBeenCalledWith({
          accessToken: expect.any(String),
        });
      });

      it("should call res.status(422) and res.json with error message", async () => {
        const id = 1;
        const name = "Jerrel";
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";
        const encryptedPassword = bcrypt.hashSync(password, 10);
        const image = "jerrel.jpg";
        const roleId = 2;

        const mockUser = new User({ id: id, name: name, email: email, image: image, password: encryptedPassword, roleId: roleId });

        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const mockRequest = {
          body: {
            name,
            email,
            password,
          },
        };

        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };

        const mockNext = jest.fn();

        const mockUserModel = { findOne: jest.fn().mockReturnValue(mockUser) };

        const authenticationController = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
        await authenticationController.handleRegister(mockRequest, mockResponse, mockNext);

        expect(mockUserModel.findOne).toHaveBeenCalledWith({
          where: { email: mockRequest.body.email },
        });
        expect(mockResponse.status).toHaveBeenCalledWith(422);
        expect(mockResponse.json).toHaveBeenCalledWith({
          message: "Maaf, email sudah terdaftar",
        });
      });

      it("should call next function with error", async () => {
        const err = new Error("Something Error");
        const name = "Jerrel";
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";

        const mockUserModel = {
          findOne: jest.fn(() => Promise.reject(err)),
        };

        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const mockRequest = {
          body: {
            name,
            email,
            password,
          },
        };

        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };

        const mockNext = jest.fn();

        const authenticationController = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
        await authenticationController.handleRegister(mockRequest, mockResponse, mockNext);

        expect(mockNext).toHaveBeenCalled();
      });
    });

    describe("#handleGetUser", () => {
      it("should call res.status(200) and res.json with user instance", async () => {
        const id = 1;
        const name = "Jerrel";
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";
        const encryptedPassword = bcrypt.hashSync(password, 10);
        const image = "jerrel.jpg";
        const roleId = 2;

        const mockUser = new User({ id: id, name: name, email: email, image: image, password: encryptedPassword, roleId: roleId });
        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const mockRequest = {
          user: {
            id: 6,
          },
        };

        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };

        const mockUserModel = {
          ...mockUser.dataValues,
          findByPk: jest.fn().mockReturnValue(mockUser),
        };

        const mockRoleModel = {
          ...mockRole.dataValues,
          findByPk: jest.fn().mockReturnValue(mockUser.roleId),
        };

        const authenticationController = new AuthenticationController({
          userModel: mockUserModel,
          roleModel: mockRoleModel,
          bcrypt,
          jwt,
        });
        await authenticationController.handleGetUser(mockRequest, mockResponse);

        expect(mockUserModel.findByPk).toHaveBeenCalledWith(mockRequest.user.id);
        expect(mockRoleModel.findByPk).toHaveBeenCalledWith(mockUser.roleId);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(mockUser);
      });
    });

    describe("#createTokenFromUser", () => {
      it("should return the result of token", async () => {
        const id = 1;
        const name = "Jerrel";
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";
        const encryptedPassword = bcrypt.hashSync(password, 10);
        const image = "jerrel.jpg";
        const roleId = 2;

        const mockUser = new User({ id: id, name: name, email: email, password: encryptedPassword, image: image, roleId: roleId });

        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const token = jwt.sign(
          {
            id: mockUser.id,
            name: mockUser.name,
            email: mockUser.email,
            image: mockUser.image,
            role: {
              id: mockRole.id,
              name: mockRole.name,
            },
          },
          JWT_SIGNATURE_KEY
        );

        const authenticationController = new AuthenticationController({
          userModel: mockUser,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
        const result = await authenticationController.createTokenFromUser(mockUser, mockRole);

        expect(result).toEqual(token);
      });
    });

    describe("#decodeToken", () => {
      it("should return the result of decoded token", async () => {
        const id = 1;
        const name = "Jerrel";
        const email = "jerrel@binar.co.id";
        const password = "jerrel123";
        const encryptedPassword = bcrypt.hashSync(password, 10);
        const image = "jerrel.jpg";
        const roleId = 2;

        const mockUser = new User({ id: id, name: name, email: email, password: encryptedPassword, image: image, roleId: roleId });

        const mockRole = new Role({ id: 2, name: "ADMIN" });

        const token = jwt.sign(
          {
            id: mockUser.id,
            name: mockUser.name,
            email: mockUser.email,
            image: mockUser.image,
            role: {
              id: mockRole.id,
              name: mockRole.name,
            },
          },
          JWT_SIGNATURE_KEY
        );

        const decodeToken = jwt.verify(token, JWT_SIGNATURE_KEY);

        const authenticationController = new AuthenticationController({
          userModel: mockUser,
          roleModel: mockRole,
          bcrypt,
          jwt,
        });
        const result = await authenticationController.decodeToken(token);

        expect(result).toEqual(decodeToken);
      });
    });

    describe("#encryptPassword", () => {
      it("should return the result of encrypted password", async () => {
        const password = "jerrel123";
        const encryptedPassword = bcrypt.hashSync(password, 10);

        const authenticationController = new AuthenticationController({
          User,
          Role,
          bcrypt,
          jwt,
        });
        const result = await authenticationController.encryptPassword(password);

        expect(result.slice(0, -53)).toEqual(encryptedPassword.slice(0, -53));
      });
    });

    describe("#verifyPassword", () => {
      it("should return the result of comparing password and encrypted password", async () => {
        const password = "jerrel123";
        const encryptedPassword = bcrypt.hashSync(password, 10);

        const verifyPassword = bcrypt.compareSync(password, encryptedPassword);

        const authenticationController = new AuthenticationController({
          User,
          Role,
          bcrypt,
          jwt,
        });
        const result = await authenticationController.verifyPassword(password, encryptedPassword);

        expect(result).toEqual(verifyPassword);
      });
    });
  });
});
